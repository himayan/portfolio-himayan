import React, { Component } from "react";
import { HashRouter as Router, Route } from "react-router-dom";
import "./App.css";
import About from "./Components/About";
import Projects from "./Components/Projects";
import NavBar from "./Components/NavBar";
class App extends Component {
  render() {
    return (
      <Router>
        <div className="App">
          <Route
            path="/"
            exact
            render={() => {
              return (
                <div>
                  <NavBar active="about" />
                  <About />
                </div>
              );
            }}
          />
          <Route
            path="/projects"
            render={() => {
              return (
                <span>
                  <NavBar active="projects" />

                  <Projects />
                </span>
              );
            }}
          />
          <Route
            path="/about"
            render={() => {
              return (
                <div>
                  <NavBar active="about" />

                  <About />
                </div>
              );
            }}
          />
        </div>
      </Router>
    );
  }
}

export default App;
