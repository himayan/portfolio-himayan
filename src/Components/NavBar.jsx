import React, { Component } from "react";
import { Navbar, Nav, NavItem } from "react-bootstrap";
import { Link } from "react-router-dom";
class NavBar extends Component {
  state = {
    project_active: "inactive",
    about_active: "inactive"
  };

  render() {
    return (
      <div className="navbar">
        <Navbar inverse collapseOnSelect>
          <Navbar.Header>
            <Navbar.Brand>
              <Link to="/">Portfolio</Link>
            </Navbar.Brand>
            <Navbar.Toggle />
          </Navbar.Header>
          <Navbar.Collapse className="navbar-collapse">
            <Nav pullRight>
              <NavItem componentClass="span" className="link-box nav-item">
                <span
                  className={
                    this.props.active === "projects" ? "active" : "inactive"
                  }
                >
                  <Link className="link" to="/projects" id="project-key">
                    Projects
                  </Link>
                </span>
              </NavItem>
              <NavItem componentClass="span" className="nav-item">
                <span
                  className={
                    this.props.active === "about" ? "active" : "inactive"
                  }
                >
                  <Link className="link" to="/about" id="about-key">
                    About me
                  </Link>
                </span>
              </NavItem>
            </Nav>
          </Navbar.Collapse>
        </Navbar>
      </div>
    );
  }
}

export default NavBar;
