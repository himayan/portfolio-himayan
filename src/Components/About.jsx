import React, { Component } from "react";

import { OverlayTrigger, Tooltip } from "react-bootstrap";
import { Link } from "react-router-dom";
const tooltip1 = <Tooltip id="tooltip">Java</Tooltip>;
const tooltip2 = <Tooltip id="tooltip">HTML5</Tooltip>;
const tooltip3 = <Tooltip id="tooltip">CSS3</Tooltip>;
const tooltip4 = <Tooltip id="tooltip">Node</Tooltip>;
const tooltip5 = <Tooltip id="tooltip">MongoDB</Tooltip>;
const tooltip6 = <Tooltip id="tooltip">JavaScript</Tooltip>;
const tooltip7 = <Tooltip id="tooltip">React</Tooltip>;
const tooltip8 = <Tooltip id="tooltip">Redux</Tooltip>;
const tooltip9 = <Tooltip id="tooltip">Bootstrap</Tooltip>;

class About extends Component {
  state = {};
  render() {
    return (
      <div className="about-container">
        <div className="about-background" />

        <div className="about">
          <div className="image">
            <img
              alt="awesome-me"
              className="profile-pic"
              src="https://lh3.googleusercontent.com/BAfENaSirwE0cWF6_VvvFk26AKWS0vqFpczx4CsjiTTljvtxdm63cwv5CASubWMRUFee9mG3tZdon6N3dwbr6IcTeUqxXb5e3QqOFd2a_LcaUYFZwPOTsL29V8uArH4PX8USYBhiXmk=w2400"
            />
          </div>
          <div className="title">
            <span className="name">Himayan Debnath</span>
          </div>
          <div className="me">
            <span className="desc">
              I am a front-end web developer and currently working at Wipro
              Limited.
              <br />I am from Kolkata and I graduated from VIT, Vellore.
              <br />
              <br />
              <div className="training">
                I was trained in the{" "}
                <strong style={{ margin: "0 0.5% 0 0.5%" }}>
                  Apple Academy
                </strong>{" "}
                in the following tools and I have built some apps implementing
                them which can be viewed right{" "}
                <Link className="to-project" to="/projects">
                  here.
                </Link>
              </div>
            </span>
          </div>

          <div className="skills">
            <div className="skill-head">Skills</div>
            <div className="skill-icon">
              <div className="icon">
                <OverlayTrigger placement="top" overlay={tooltip1}>
                  <i className="fab fa-java" />
                </OverlayTrigger>
              </div>
              <div className="icon">
                <OverlayTrigger placement="top" overlay={tooltip2}>
                  <i className="fab fa-html5" />
                </OverlayTrigger>
              </div>
              <div className="icon">
                <OverlayTrigger placement="top" overlay={tooltip3}>
                  <i className="fab fa-css3-alt" />
                </OverlayTrigger>
              </div>
              <div className="icon">
                <OverlayTrigger placement="top" overlay={tooltip9}>
                  <img
                    className="bootstrap-img"
                    src="https://image.ibb.co/gPyyVL/image.png"
                    alt="bootstrap"
                    border="0"
                    height="40"
                    width="40"
                    style={{
                      marginBottom: "10%"
                    }}
                  />
                </OverlayTrigger>
              </div>
              <div className="icon">
                <OverlayTrigger placement="top" overlay={tooltip4}>
                  <i className="fab fa-node" />
                </OverlayTrigger>
              </div>
              <div className="icon">
                <OverlayTrigger placement="top" overlay={tooltip5}>
                  <img
                    src="https://image.ibb.co/ikr7qL/mongodb-2.png"
                    alt="mongodb-1"
                    border="0"
                    height="30"
                    width="30"
                  />
                </OverlayTrigger>
              </div>
              <div className="icon">
                <OverlayTrigger placement="top" overlay={tooltip6}>
                  <i className="fab fa-js-square" />
                </OverlayTrigger>
              </div>
              <div className="icon">
                <OverlayTrigger placement="top" overlay={tooltip7}>
                  <i className="fab fa-react" />
                </OverlayTrigger>
              </div>
              <div className="icon">
                <OverlayTrigger placement="top" overlay={tooltip8}>
                  <img
                    src="https://preview.ibb.co/iZGMjf/redux-logo-black-and-white.png"
                    alt="redux-logo-black-and-white"
                    border="0"
                    height="30"
                    width="30"
                  />
                </OverlayTrigger>
              </div>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

export default About;
