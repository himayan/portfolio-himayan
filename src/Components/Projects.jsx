import React, { Component } from "react";
import { Panel } from "react-bootstrap";
import { Modal } from "react-bootstrap";

class Projects extends Component {
  state = {
    video: false,
    show: false,
    id: 0
  };
  title = [
    "To Do List Demo",
    "Bookstore Demo",
    "Weather App Demo",
    "Online Test Demo"
  ];
  vid_id = [
    "https://player.vimeo.com/video/300490650?title=0&byline=0&portrait=0",
    "https://player.vimeo.com/video/300492070?title=0&byline=0&portrait=0",
    "https://player.vimeo.com/video/300705265?title=0&byline=0&portrait=0",
    "https://player.vimeo.com/video/300708908?title=0&byline=0&portrait=0"
  ];
  img_id = [
    "https://lh3.googleusercontent.com/caupsoOHCD7-bXRFPiWpN-Ho8eTNhuWDDka8ulShLitArESNgGFAkTsIq4IWrWxkrPFg0PH6ENQxpFBDFuOdb3F-v06jrOqUKYGft82glyu5Yl4xiVKEswee_UnONUOv9oFo5E_RkFk=w2400",
    "https://lh3.googleusercontent.com/vuxpiLJd1R9-k-Tbs0MYSsZ69Wf4Qo8kd_iI64iLYXAMAFVFTEyp_quqR2O-Bden1KJ7QlTHzYIpMfQBIX8z911gkmnQigp_LmqbFFMJurf7JyB2-CPiqMepxKWNFbWLDUscrjG3Y0o=w2400",
    "https://lh3.googleusercontent.com/vWIgir3u4_RgzcXOzvK4Qzk7dtOVH66jK9T4ZbpB4gf6IDulV9DCmcgezCAw1tKVNIEobyQTzyvEyELmEcxuwoAjaID7EJ6ndpEjcTMwAfdGhR7FodMfl2lgamtLSizBEDZONrLyogA=w2400",
    "https://lh3.googleusercontent.com/OVm9b9nJCITlesrJJNi-AkpyUE0UDNA-qlLCFU3fVcJwCmahKAmlmEVqJ3L5mQVtgOHw3mZlNsWgIzqMYuW-cEc-vuQIzFgyNJijFq-KtF-vi6D8pztJ7WkBgpinpao7UnD8DkiwKKA=w2400"
  ];
  handleClose() {
    this.setState({ show: false });
    this.setState({ video: false });
  }
  handleVideo(e) {
    this.setState({ video: true });
    this.setState({ id: e.target.id });
  }
  handleShow(e) {
    this.setState({ show: true });
    this.setState({ id: e.target.id });
  }

  render() {
    return (
      <div className="components">
        <div className="project-background" />
        <Modal
          show={this.state.video}
          onHide={this.handleClose.bind(this)}
          bsSize="large"
          dialogClassName="my-modal"
        >
          <Modal.Header closeButton>
            <Modal.Title id="contained-modal-title-sm">
              {this.title[this.state.id - 1]}
            </Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <div style={{ padding: "56.25% 0 0 0", position: "relative" }}>
              <iframe
                title="video"
                src={this.vid_id[this.state.id - 1]}
                style={{
                  position: "absolute",
                  top: 0,
                  left: 0,
                  width: "100%",
                  height: "100%"
                }}
                frameBorder="0"
                allowFullScreen
              />
            </div>
          </Modal.Body>
        </Modal>
        <Modal show={this.state.show} onHide={this.handleClose.bind(this)}>
          <Modal.Header closeButton>
            <Modal.Title>Image Preview</Modal.Title>
          </Modal.Header>
          <Modal.Body>
            <img
              className="modal-image"
              src={this.img_id[this.state.id - 1]}
              alt="Screen-Shot-2018-11-12-at-2-51-39-PM"
              border="0"
            />
          </Modal.Body>
        </Modal>

        <div className="list">List of Projects</div>

        <div className="list-container">
          <div className="project-list indi-pro">
            <Panel defaultExpanded>
              <Panel.Heading
                style={{
                  backgroundColor: "black",
                  color: "white",
                  opacity: 0.8,
                  height: 100,
                  verticalAlign: "center"
                }}
                className="project-header"
              >
                <Panel.Title className="project-title">To-Do List</Panel.Title>
                <Panel.Toggle componentClass="a">
                  <span id="button">-</span>
                </Panel.Toggle>
              </Panel.Heading>
              <Panel.Collapse>
                <Panel.Body className="project-desc">
                  A to-do list for you to revisit once in a while so that you
                  never forget to get anything done on time.
                  <br />
                  <div className="wrapper-skill">
                    <div className="tools">Tools</div>
                    <div className="skillset">
                      {" "}
                      ReactJS, Redux, JavaScript, CSS, Bootstrap
                    </div>
                  </div>
                  <img
                    id={1}
                    onClick={this.handleShow.bind(this)}
                    className="project-img"
                    src="https://preview.ibb.co/k1OzXq/Screen-Shot-2018-11-12-at-2-51-39-PM.png"
                    alt="Screen-Shot-2018-11-12-at-2-51-39-PM"
                    border="0"
                  />
                  <br />
                  <button
                    className="details"
                    id={1}
                    onClick={this.handleVideo.bind(this)}
                  >
                    View Demo
                  </button>
                </Panel.Body>
              </Panel.Collapse>
            </Panel>
          </div>
          <div className="project-list indi-pro-2">
            <Panel defaultExpanded>
              <Panel.Heading
                style={{
                  backgroundColor: "black",
                  color: "white",
                  opacity: 0.8,
                  height: 100
                }}
                className="project-header"
              >
                <Panel.Title className="project-title">
                  e-commerce Bookstore
                </Panel.Title>
                <Panel.Toggle componentClass="a">
                  <span id="button">-</span>
                </Panel.Toggle>
              </Panel.Heading>
              <Panel.Collapse>
                <Panel.Body className="project-desc">
                  A digital store to find your favorite book with the option of
                  adding it to your cart and price comparison.
                  <br />
                  <div className="wrapper-skill">
                    <div className="tools">Tools</div>
                    <div className="skillset">
                      {" "}
                      ReactJS, JavaScript, CSS, Bootstrap
                    </div>
                  </div>
                  <img
                    id={2}
                    onClick={this.handleShow.bind(this)}
                    className="project-img2"
                    src="https://preview.ibb.co/naCF5A/Screen-Shot-2018-11-12-at-5-45-25-PM.png"
                    alt="Screen-Shot-2018-11-12-at-5-45-25-PM"
                    border="0"
                  />
                  <button
                    id={2}
                    className="details"
                    onClick={this.handleVideo.bind(this)}
                  >
                    View Demo
                  </button>
                </Panel.Body>
              </Panel.Collapse>
            </Panel>
          </div>
          <div className="project-list indi-pro-3">
            <Panel defaultExpanded>
              <Panel.Heading
                style={{
                  backgroundColor: "black",
                  color: "white",
                  opacity: 0.8,
                  height: 100
                }}
                className="project-header"
              >
                <Panel.Title className="project-title">Weather app</Panel.Title>
                <Panel.Toggle componentClass="a">
                  <span id="button">-</span>
                </Panel.Toggle>
              </Panel.Heading>
              <Panel.Collapse>
                <Panel.Body className="project-desc">
                  A weather app to give you real time weather information of any
                  location around the world.
                  <br />
                  <div className="wrapper-skill">
                    <div className="tools">Tools</div>
                    <div className="skillset">
                      {" "}
                      HTML5, JavaScript, CSS, Bootstrap
                    </div>
                  </div>
                  <img
                    id={3}
                    onClick={this.handleShow.bind(this)}
                    className="project-img3"
                    src="https://preview.ibb.co/bLyPXq/Screen-Shot-2018-11-12-at-5-54-29-PM.png"
                    alt="Screen-Shot-2018-11-12-at-5-54-29-PM"
                    border="0"
                  />
                  <button
                    id={3}
                    className="details"
                    onClick={this.handleVideo.bind(this)}
                  >
                    View Demo
                  </button>
                </Panel.Body>
              </Panel.Collapse>
            </Panel>
          </div>
          <div className="project-list indi-pro-4">
            <Panel defaultExpanded>
              <Panel.Heading
                style={{
                  backgroundColor: "black",
                  color: "white",
                  opacity: 0.8,
                  height: 100
                }}
                className="project-header"
              >
                <Panel.Title className="project-title">
                  Online Test App
                </Panel.Title>
                <Panel.Toggle componentClass="a">
                  <span id="button">-</span>
                </Panel.Toggle>
              </Panel.Heading>
              <Panel.Collapse>
                <Panel.Body className="project-desc">
                  A digital platform for users to appear for exams with an admin
                  side to overlook scores and details.
                  <br />
                  <div className="wrapper-skill">
                    <div className="tools">Tools</div>
                    <div className="skillset">
                      {" "}
                      ReactJS, Redux, JavaScript, CSS, Bootstrap, Mongo
                    </div>
                  </div>
                  <img
                    id={4}
                    onClick={this.handleShow.bind(this)}
                    className="project-img4"
                    src="https://preview.ibb.co/keMw0L/Screen-Shot-2018-11-14-at-3-05-08-PM.png"
                    alt="Screen-Shot-2018-11-12-at-5-54-29-PM"
                    border="0"
                  />
                  <button
                    id={4}
                    className="details"
                    onClick={this.handleVideo.bind(this)}
                  >
                    View Demo
                  </button>
                </Panel.Body>
              </Panel.Collapse>
            </Panel>
          </div>
        </div>
      </div>
    );
  }
}

export default Projects;
